# Stabilita diskrétního linearního systému

Uvažujeme-li lineární systém ve tvaru
```math
\bm{x}_{k} = \bm{A} \bm{x}_{k-1} ,\quad \bm{A} ∈ \mathbb{R}^{n×n},\ \bm{x} ∈ \mathbb{R}^n 
```

jeho stav v k-tém časovém kroku lze popsat rovnicí
```math
\bm{x}_{k} = \bm{A}^k \bm{x}_0 
```

---

Jsou-li $`λ_i ∈ \mathbb{C}`$ vlastní čísla a $`\bm{u}_i ∈ \mathbb{R}^n`$ vlastní vektory $`\bm{A}`$:
```math
\bm{A} \bm{u}_i = λ_i \bm{u}_i ,\quad i ∈ ⟨1,n⟩
```

matici $`\bm{A}`$ lze vyjádřit jako:
```math
\bm{A} = \bm{T}\bm{D}\bm{T}^{-1} ,\quad \bm{T} = [u_i], \ \bm{D} = ⌈λ_i⌋
```

Dosazením obdržíme stav v k-tém kroku:
```math
\bm{x}_{k} = \bm{T} \bm{D}^k \bm{T}^{-1} \bm{x}_0 ,\quad \bm{D}^k = ⌈λ_i^k⌋ 
```

---

ze kterého plyne podmínka stability
```math
\bm{x}_k → 0 ⇔ |λ_i| ≤ 1,\ ∀i
```

## Souvisí:
[stabilita kontinuálního linearního systému](StabilitaSystemu-konti.md)
