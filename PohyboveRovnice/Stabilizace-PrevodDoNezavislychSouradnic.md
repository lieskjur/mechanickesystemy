Předpokládejme pohybové rovnice ve tvaru:
```math
	\bm{M}\ddot{\bm{s}}
	=
	\bm{\Phi}_s^T \bm{\lambda} +
	\bm{p}(\bm{s},\dot{\bm{s}})
```
a nezávislé souřadnice $`\bm{q}`$ zvolené:
```math
	\bm{q} = \bm{B}\bm{s} \;,\quad 
	\dot{\bm{q}} = \bm{B}\dot{\bm{s}} \;,\quad 
	\ddot{\bm{q}} = \bm{B}\ddot{\bm{s}}
```
Sestavíme-li soustavy rovnic:
```math
	\begin{bmatrix}
		\bm{0} \\ \dot{\bm{q}}
	\end{bmatrix}
	=
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}
	\dot{\bm{s}} 
	\;,\quad 
	\begin{bmatrix}
		\bm{0} \\ \ddot{\bm{q}}
	\end{bmatrix}
	=
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}
	\ddot{\bm{s}}
	+
	\begin{bmatrix}
		\dot{\bm{\Phi}}_s \\ \bm{0}
	\end{bmatrix}
	\dot{\bm{s}}
```
lze vyjádřit první a druhou derivaci přirozených souřadnic podle času jako:
```math
	\dot{\bm{s}}
	=
	\bm{R}\dot{\bm{q}}
	\;,\quad 
	\ddot{\bm{s}}
	=
	\bm{R}\ddot{\bm{q}}
	+
	\dot{\bm{R}}\dot{\bm{q}}
```
kde
```math
	\begin{bmatrix}
		\bm{R}^* & \bm{R}
	\end{bmatrix}
	=
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}^{-1}
```
a
```math
	\begin{bmatrix}
		\dot{\bm{R}}^* & \dot{\bm{R}}
	\end{bmatrix}
	=
	-
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}^{-1}
	\begin{bmatrix}
		\dot{\bm{\Phi}}_s \\ \bm{0}
	\end{bmatrix}
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}^{-1}
```

---
Po dosazení a vynásobením $`\bm{R}^T`$ z leva získáme pohybové rovnice
```math
	\bm{R}^T\bm{M}\bm{R}\ddot{\bm{q}}
	=
	\bm{R}^T\bm{p}(\bm{s},\dot{\bm{s}}) -
	\bm{R}^T\bm{M}\dot{\bm{R}}\dot{\bm{q}}
```
jelikož
```math
	\bm{R}^T\bm{\Phi}^T = \bm{0}
```

---
Zrychlení v přirozených souřadnicích lze zpětně získat ze vztahu
```math
	\ddot{\bm{s}}
	=
	\bm{R}(\bm{R}^T\bm{M}\bm{R})^{-1}
	\bm{R}^T \big(\bm{p}(\bm{s},\dot{\bm{s}}) - \bm{M}\dot{\bm{R}}\dot{\bm{q}}\big)
	+
	\dot{\bm{R}}\dot{\bm{q}}
```
<!--
```math
	\dot{\bm{s}}
	=
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}^{-1}
	\begin{bmatrix}
		\bm{0} \\ \dot{\bm{q}}
	\end{bmatrix}
	=
	\bm{R} \dot{\bm{q}}
	\;,\quad 
	\ddot{\bm{s}}
	=
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}^{-1}
	\begin{bmatrix}
		\bm{0} \\ \ddot{\bm{q}}
	\end{bmatrix}
	-
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}^{-1}
	\begin{bmatrix}
		\dot{\bm{\Phi}}_s \\ \bm{0}
	\end{bmatrix}
	\begin{bmatrix}
		\bm{\Phi}_s \\ \bm{B}
	\end{bmatrix}^{-1}
	\begin{bmatrix}
		\bm{0} \\ \dot{\bm{q}}
	\end{bmatrix}
```
-->