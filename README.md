# Mechanické Systémy

## Equations of Motion of a constrained multibody system
- [Introduction](EoM/Constrained-MBS-EoM.md)
- [Explicit Motion Constraints](EoM/ExplicitMotionConstraints.md)
- [Implicit Motion Constraints](EoM/ImplicitMotionConstraints.md)

## Stabilita lineárních systémů
- [Kontinuální systém](StabilitaSystemu-konti.md)
- [Diskrétní systém](StabilitaSystemu-diskr.md)

## Řízení
- [Základní pojmy](Rizeni/ZakladniPojmyRizeni.md)

### Optimální řízení
- [Cílový funkcionál](Rizeni/Optimalni/CilovyFunkcional.md)
- [Pontrjaginův princip maxima](Rizeni/Optimalni/PontrjaginuvPrincipMaxima.md)

### Robustní řízení
- [Převod stavového popisu na Přenos systému](RobustniRizeni/PrevodStavovyPrenosovyPopis.md)
- [Citlivost systému](Rizeni/Robustni/Citlivost.md)

### Zpětnovazebná regulace
- [Computed Torques](Rizeni/ComputedTorques.md)

## Doprovodná matematika
- [Numerická aproximace derivací prvního řádu](Matematika/NumDerivacePrvnihoRadu.md)