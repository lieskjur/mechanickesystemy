# Numerické aproximace derivace prvního řádu

## Konečnou diferencí
```math
f'(x) = \frac{f(x+h)-f(x)}{h} + \mathcal{O}(h^2)
```

## Metodou komplexního kroku
```math
f'(x) = \frac{\text{Im}\big(f(x+ih)\big)}{h} + \mathcal{O}(h^2)
```

## Metodou duálních čísel
<!--f(x+yε) = f(x) + f'(x)yε + \frac{f''(x)}{2!} y^2 ε^2 + ...-->
```math
f'(x) = \text{Eps}\big(f(x+ε)\big) \,,\quad ε≠0,\ ε^2=0
```
