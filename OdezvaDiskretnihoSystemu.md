# Odezva řízeného diskrétního systému na impulz

```math
x_{k+1}  = Ax_k + Bu_k 
```

```math
\begin{align*}
x &= 3 \\
y &= 4
\end{align*}
```

```math
\begin{align*}
	u_0 &= 1 	&, x_0	&= 0  		&\Rightarrow x_1		&= B \\
	u_1 &= 0 	&, x_1	&= B  		&\Rightarrow x_2		&= AB \\
	u_2 &= 0 	&, x_2	&= AB 		&\Rightarrow x_3		&= A^2B \\
		&⋮		&		&⋮			&			&⋮	\\
	u_k &= 0 	&, x_k 	&= A^{k-1}B &\Rightarrow x_{k+1} 	&= A^kB
\end{align*}
```

