# Algebraic Riccati Equation

## Continuous time
```math
	\bm{A}^T \bm{P} + \bm{P} \bm{A} - \bm{P} \bm{B} \bm{R}^{-1} \bm{B}^T \bm{P} + \bm{Q} = \bm{0}
```
```math
\bm{K} = \bm{R}^{-1} \bm{B}^T \bm{P}
```

## Discrete time
```math
\bm{P} = \bm{A}^T \bm{P} \bm{A} - (\bm{A}^T \bm{P} \bm{B})(\bm{R} + \bm{B}^T \bm{P} \bm{B})^{-1} (\bm{B}^T \bm{P} \bm{A}) + \bm{Q}
```
```math
\bm{K} = (\bm{R} + \bm{B}^T \bm{P} \bm{B})^{-1} \bm{B}^T \bm{P} \bm{A}
```

Generally we search for the unique stabilizing solution