# Computed torques

Pro mechanický systém popsán pohybovými rovnicemi ve tvaru:
```math
Mq̈ + C = τ + G 
```

lze vyjádřit potřebný zásah pohonů $`τ`$ pro dosažení zrychlení $`q̈`$ ve tvaru:
```math
τ = Mq̈ + C - G 
```

Odchylku $`e`$ od žádané polohy lze regulovat pomocí vyjádření zrychlení $`q̈`$
jako součtu žádaného zrychlení $`q̈_d`$ a složek PID:
```math
q̈ = q̈_d + K_v ė + K_p e + K_i ε \,,\quad ε = ∫_0^t e\,dt 
```

Dosazením získáme vztah pro zásah pohonů:
```math
τ = M(q̈_d + K_v ė + K_p e + K_i ε) + C - G 
```