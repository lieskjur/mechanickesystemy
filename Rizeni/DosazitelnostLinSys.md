# Dosažitelnost lineárního systému

Stav lineárního systému $`\dot{\bm{x}} = \bm{A}\bm{x} + \bm{B}\bm{u}`$ v čase $`t`$ je
```math
\bm{x}(t) = \int_0^t e^{\bm{A}(t-τ)} \bm{B} \bm{u}(τ)\ dτ
```
Cayley-Hamiltonův teorém praví, že pro rozumnou matici $`\bm{A}`$ lze exponenciání funkci $`e^{\bm{A}t}`$ vyjádřit jako:
```math
e^{\bm{A}t} = ϕ_0(t) \bm{I} + ϕ_1(t) \bm{A} + \dots + ϕ_{n-1}(t) \bm{A}^{n-1}
```

Po dosazení obdržíme rovnici
```math
\bm{x}(t) = \int_0^t \big( ϕ_0(t-τ)\ \bm{u}(τ) \bm{I} \bm{B} + ϕ_1(t-τ)\ \bm{u}(τ) \bm{A} \bm{B} + \dots + ϕ_{n-1}(t-τ)\ \bm{u}(τ) \bm{A}^{n-1} \bm{B} \big)\ dτ
```

kterou lze dále upravit do maticového tvaru
```math
\bm{x}(t) =
\bm{\bm{R}}
\int_0^t
\begin{bmatrix}
	ϕ_0(t-τ)\ \bm{u}(τ)\\
	ϕ_1(t-τ)\ \bm{u}(τ)\\
	\vdots \\
	ϕ_{n-1}(t-τ)\ \bm{u}(τ)\\
\end{bmatrix}
dτ
\,,\quad
\bm{\bm{R}}
=
\begin{bmatrix}
	\bm{B} & \bm{A}\bm{B} & \dots & \bm{A}^{n-1}\bm{B}
\end{bmatrix}
```
, kde $`\bm{R}`$ je matice dosažitelnosti.

---

Všechny stavy systému $`\bm{x} ∈ \mathbb{R}^n`$ jsou dosažitelné vhodnou volbou řízení $`\bm{u}`$ právě tehdy, když:
```math
\text{rank}(\bm{R}) = n
```