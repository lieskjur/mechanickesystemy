# Převod stavového popisu systému na přenosový popis

### Výchozí stavový popis
```math
\begin{align*}
\dot{\bm{x}} &= \bm{A} \bm{x} + \bm{B} \bm{u} \\
\bm{y} &= \bm{C} \bm{x}
\end{align*}
```
<!-- ,kde $`\bm{x}`$ jsou stavové proměnné, $`\bm{u}`$ vstupy a $`\bm{y}`$ výstupy systému. -->

### Přenos systému
```math
\bm{G}(s) = \bm{Y}(s)\,\bm{U}(s)^{-1}
```

## Převod

Laplaceovou transformací stavového popisu obdržíme:
```math
\begin{align*}
s \bm{X}(s) &= \bm{A} \bm{X}(s) + \bm{B} \bm{U}(s) \\
\bm{Y}(s) &= \bm{C} \bm{X}(s)
\end{align*}
```

Po úpravě a dosazení získame přenosový popis ve tvaru:
```math
\bm{G}(s) = \bm{C} ( s\bm{I} - \bm{A} )^{-1} \bm{B}
```

<!--
```math
\dot{X} → s X(s)
```
-->