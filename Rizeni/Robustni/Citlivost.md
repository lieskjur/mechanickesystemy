# Citlivost a komplementární citlivost systému

Závislost výstupu y, otevřené smyčky regulovaného systému, na odchylce ε od žádaného stavu r je
```math
\bm{y} = \bm{P}_d\,\bm{d} + \bm{L}\,\bm{ε} \,,\quad \bm{L} = \bm{P}\bm{K},\ \bm{ε} = \bm{r}-\bm{y}-\bm{n}
```
, kde d jsou .... a n ... 

Po převodu y na pravou stranu obdržíme soustavu rovnic:
```math
(\bm{I}+\bm{L})\,\bm{y} = \bm{P}_d\,\bm{d} + \bm{L}\,\bm{r} - \bm{L}\,\bm{n}
```

Citlivost \bm{S} a k ní *komplementární citlivost* \bm{T} řízeného systému definujeme jako :
```math
\begin{align*}
\bm{S} &= (\bm{I}+\bm{L})^{-1} \\
\bm{T} &= (\bm{I}+\bm{L})^{-1}\bm{L}
\end{align*}
```
kdy
```math
\bm{T}+\bm{S} = \bm{I}
```

Výstup systému lze následně popsat jako součet součinů citlivostí S,T a proměnných r,d,n
```math
	\bm{y} = \bm{T}\bm{r} + \bm{S}\bm{P}_d\,\bm{d} - \bm{T}\bm{n}
```

<!--
ε = r - y_m = Sr - SP_d d + T n
-->