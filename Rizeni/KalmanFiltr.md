# Kalmanův filtr

Uvažujeme-li lineární systém s
```math
\begin{align*}
\dot{x} &= A x + B u + w_d \\
y &= C x + w_n
\end{align*}
```

* $`w_d`$ - Gaussian (disturbance)
* $`v_d`$ - Variance
* $`w_n`$ - Gaussian (noise)
* $`v_n`$ - Variance

Kritérium optimality
```math
J = E ε^T ε
```

$`E`$ - expectation value

<!--
lqe(A,V_d,C,V_d,V_n) = lqr(A',C',V_d,V_n)'
-->