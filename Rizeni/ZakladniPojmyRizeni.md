# Základní pojmy
<!--
Předpokládejmě řízený systém v obecném tvaru
```math
\begin{align*}
	\dot{\bm{x}} &= \bm{f}(\bm{x},\bm{u},t) \\
	\bm{y} &= \bm{g}(\bm{x})
\end{align*}
```
-->
### Dosažitelnost
Systém je dosažitelný pokud jej lze z nulového stavu  $`\bm{x} = \bm{0}`$ dostat do libovolného nenulového stavu $`\bm{x} ∈ \mathbb{R}^n`$ působením vstupů $`\bm{u}`$.

### Říditelnost
Systém je říditelný pokud jej lze z libovolného stavu $`\bm{x} ∈ \mathbb{R}^n`$ dostat do stavu nulového $`\bm{x} = \bm{0}`$ působením vstupů $`\bm{u}`$.

### Pozorovatelnost
Systém je pozorovatelný pokud na konečném časovém intervalu lze ze změřeného průběhu vstupů $`\bm{u}`$ a výstupů $`\bm{y}`$ určit stav systému na počátku invervalu $`\bm{x}_0 ∈ \mathbb{R}^n`$.

### Rekonstruovatelnost
Systém je rekonstruovatelný pokud na konečném časovém intervalu lze ze změřeného průběhu vstupů $`\bm{u}`$ a výstupů $`\bm{y}`$ určit stav systému na konci intervalu $`\bm{x} ∈ \mathbb{R}^n`$.
