# Stavový Pozorovatel <!-- Full-state estimation -->
```math
\frac{d}{dt}\hat{\bm{x}} = \bm{A} \hat{\bm{x}} + \bm{B} \bm{u} + \bm{L} ( \bm{y} - \hat{\bm{y}} ) \,,\quad \hat{\bm{y}} = \bm{C} \hat{\bm{x}}
```
```math
\frac{d}{dt}\hat{\bm{x}} = (\bm{A}-\bm{L} \bm{C})\,\hat{\bm{x}} + \bm{B} \bm{u} + \bm{L} \bm{y}
```

```math
\dot{\bm{ε}} = (\bm{A} - \bm{L} \bm{C} )\,\bm{ε} \,,\quad \bm{ε}=\bm{x}-\hat{\bm{x}}
``` 