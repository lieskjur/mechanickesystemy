# Cílový funkcionál <!--objective functional-->
```math
J = \int_0^T L(\bm{x},\bm{u},t)\,dt
```

Lagrangián
- $`L = \bm{u}^T \bm{R}\,\bm{u}`$ - spotřeby energie
- $`L = \bm{x}^T \bm{Q}\,\bm{x}`$ - odchýlení od počátku
- $`L = \|\bm{u}\|`$ - spotřeby paliva
- $`L = 1`$ - času