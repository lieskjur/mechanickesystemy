# Hybrid Dynamics
```math
\begin{bmatrix}
	\bm{H}_{FF} & \bm{H}_{FI} \\
	\bm{H}_{IF} & \bm{H}_{II}
\end{bmatrix}
\begin{bmatrix}
    \ddot{\bm{q}}_F \\ \ddot{\bm{q}}_I
\end{bmatrix}
+
\begin{bmatrix}
    \bm{C}_F \\ \bm{C}_I
\end{bmatrix}
=
\begin{bmatrix}
    \bm{\tau}_F \\ \bm{\tau}_I
\end{bmatrix}
```
The unknown being $`\ddot{\bm{q}}_F`$ and $`\bm{\tau}_I`$ we will bring them together on the left hand side
```math
\begin{bmatrix}
	\bm{H}_{FF} & \bm{0} \\
	\bm{H}_{IF} & -\bm{1}
\end{bmatrix}
\begin{bmatrix}
	\ddot{\bm{q}}_F \\ \bm{\tau}_I
\end{bmatrix}
=
\begin{bmatrix}
	\bm{1} & -\bm{H}_{FI} \\
	\bm{0} & -\bm{H}_{II}
\end{bmatrix}
\begin{bmatrix}
	\bm{\tau}_F \\ \ddot{\bm{q}}_I
\end{bmatrix}
-
\begin{bmatrix}
    \bm{C}_F \\ \bm{C}_I
\end{bmatrix}
```
<!-- which can be further manipulated into:
```math
\begin{bmatrix}
	\bm{H}_{FF} & \bm{0} \\
	\bm{H}_{IF} & -\bm{1}
\end{bmatrix}
\begin{bmatrix}
	\ddot{\bm{q}}_F \\ \bm{\tau}_I
\end{bmatrix}
=
\begin{bmatrix}
	\bm{\tau}_F \\ \bm{0}
\end{bmatrix}
-
\begin{bmatrix}
    \bm{C}'_F \\ \bm{C}'_I
\end{bmatrix}
```
where
```math
\begin{bmatrix}
    \bm{C}'_F \\ \bm{C}'_I
\end{bmatrix}
=
\begin{bmatrix}
	\bm{C}_F + \bm{H}_{FI} \ddot{\bm{q}}_I \\
	\bm{C}_I + \bm{H}_{II} \ddot{\bm{q}}_I
\end{bmatrix}
``` -->
We may split the solution into two parts. First attaining the acceleration variables:
<!-- ```math
\ddot{\bm{q}}_F = \bm{H}_{FF}^{-1} (\bm{\tau}_F - \bm{C}'_F)
``` -->
```math
\ddot{\bm{q}}_F = \bm{H}_{FF}^{-1} (\bm{\tau}_F - \bm{H}_{FI} \ddot{\bm{q}}_I - \bm{C}_F)
```
Then the force variables:
<!-- ```math
\bm{\tau}_I = \bm{H}_{IF} \ddot{\bm{q}}_F + \bm{C}'_I
``` -->
```math
\bm{\tau}_I = \bm{H}_{IF} \ddot{\bm{q}}_F + \bm{H}_{II} \ddot{\bm{q}}_I + \bm{C}_I
```

<!--
Substituted and manipulated
```math
\bm{\tau}_I
=
\Big( \bm{H}_{II} - \bm{H}_{IF} \bm{H}_{FF}^{-1} \bm{H}_{FI} \Big) \ddot{\bm{q}}_I
+
\bm{H}_{IF} \bm{H}_{FF}^{-1} (\bm{\tau}_F - \bm{C}_F) + \bm{C}_I
```
-->
