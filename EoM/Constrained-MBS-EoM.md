# Equations of Motion of a constrained multibody system

Given a system of $`N`$ unconstrained bodies with EoM in the form:
```math
	\bm{H}_i \ddot{\bm{q}}_i + \bm{C}_i = \bm{\tau}_i \;,\quad i ∈ ⟨1,N⟩
```
we may enforce motion constraints among them using a *constraint force vector* $`\bm{\tau}_c`$ resulting in a system of equations
```math
	\begin{bmatrix}
		\bm{H}_1 	& \bm{0}	& \dots & \bm{0} \\
		\bm{0}		& \bm{H}_2	& \dots & \bm{0} \\
		⋮			& ⋮			& ⋱ 	& ⋮ \\
		\bm{0}		& \bm{0}	& \dots & \bm{H}_N
	\end{bmatrix}
	\begin{bmatrix}
		\ddot{\bm{q}}_1 \\ \ddot{\bm{q}}_2 \\ ⋮ \\ \ddot{\bm{q}}_N
	\end{bmatrix}
	+
	\begin{bmatrix}
		\bm{C}_1 \\ \bm{C}_2 \\ ⋮ \\ \bm{C}_N
	\end{bmatrix}
	=
	\begin{bmatrix}
		\bm{\tau}_1 \\ \bm{\tau}_2 \\ ⋮ \\ \bm{\tau}_N
	\end{bmatrix}
	+
	\bm{\tau}_c
```
written in a compact form as:
```math
	\bm{H} \ddot{\bm{q}} + \bm{C} = \bm{\tau} + \bm{τ}_c
```

## Jourdain's principle of virtual power
*The constraint force delivers zero power along every direction of velocity freedom that is compatible with the motion constraints.*

Therefore $`\bm{\tau}_c`$ must satisfy:
```math
\bm{τ}_c ⋅ \dot{\bm{q}} = 0
```
for all values of $`\dot{\bm{q}}`$ permitted by the motion constraints.

## Forms of motion constraints
Motion constraints can be expressed in an [implicit](ImplicitMotionConstraints.md) or [explicit](ExplicitMotionConstraints.md) form.