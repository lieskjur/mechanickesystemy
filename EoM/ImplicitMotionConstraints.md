# Implicit motion constraints

For a given constrained multibody system with equations of motion in the form:
```math
	\bm{H} \ddot{\bm{q}} + \bm{C} = \bm{\tau} + \bm{τ}_c
```
implicit **postion, velocity** and **acceleration** motion constraints are:
```math
ϕ(\bm{q}) = \bm{0} \;,\quad \bm{K}\dot{\bm{q}} = \bm{0} \;,\quad \bm{K}\ddot{\bm{q}} = \bm{k}			
```
where:
```math
\bm{K} = \frac{∂ϕ}{∂\bm{q}} \;,\quad \bm{k} = -\dot{\bm{K}}\dot{\bm{q}}
```

---
The constrained force vector $`\bm{\tau}_c`$ takes the from:
```math
\bm{\tau}_c = \bm{K}^T \bm{λ}
```
where $`\bm{\lambda}`$ is a vector of unknown force variables, satisfying the condition derived from the [Jourdain's principle of virtual power](Constrained-MBS-EoM.md):
```math
\bm{τ}_c ⋅ \dot{\bm{q}} = \bm{\lambda}^T \bm{K} \dot{\bm{q}} \overset{!}{=} 0 \;,\quad ∀\bm{\lambda}
```
for all permited $`\dot{\bm{q}}`$ as well as the velocity motion constraints.

---
The original equations of motion with $`\bm{\tau}_c`$ substituted along with the velocity motion constraint form a system of equations:
```math
\begin{bmatrix}
	\bm{H} & \bm{K}^T \\
	\bm{K} & \bm{0}
\end{bmatrix}
\begin{bmatrix}
	\ddot{\bm{q}} \\ -\bm{\lambda}
\end{bmatrix}
= 
\begin{bmatrix}
	\bm{\tau} - C \\ \bm{k}
\end{bmatrix}
```
