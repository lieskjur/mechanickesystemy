# Explicit Motion Constraints

For a given constrained multibody system with equations of motion in the form:
```math
	\bm{H} \ddot{\bm{q}} + \bm{C} = \bm{\tau} + \bm{τ}_c
```
explicit **postion, velocity** and **acceleration** motion constraints are:
```math
\begin{align*}
	\bm{q} = γ(\bm{y}) \;,\quad \dot{\bm{q}} = \bm{G}\dot{\bm{y}} \;,\quad \ddot{\bm{q}} = \bm{G}\ddot{\bm{y}} + \bm{g}
\end{align*}			
```
where:
```math
\bm{G} = \frac{∂γ}{∂\bm{y}} \;,\quad \bm{g} = \dot{\bm{G}}\bm{y}
```

---
Accoring to [Jourdain's principle of virtual power](Constrained-MBS-EoM.md):
```math
\bm{G}^T \bm{\tau}_c = \bm{0}
```
as
```math
\bm{τ}_c ⋅ \dot{\bm{q}} = \dot{\bm{y}}^T \bm{G}^T \bm{τ}_c \overset{!}{=} 0 \;,\quad ∀\dot{\bm{y}}
```

---
Substituting $`\ddot{\bm{q}}`$ and multiplying the original equations of motion by $`\bm{G}^T`$ from the left, we can reduce the number of equation while also eliminating $`\bm{\tau}_c`$ from the equations:
```math
	\bm{G}^T \bm{H} (\bm{G}\ddot{\bm{y}} + \bm{g}) + \bm{G}^T \bm{C} = \bm{G}^T \bm{\tau}
```
<!-- + \underbrace{\bm{G}^T \bm{\tau}_c}_{\bm{0}}-->
Through manipulation we can obtain:
```math
	\underbrace{\bm{G}^T\bm{H}\bm{G} \vphantom{()} }_{\bm{H}_G}\;\ddot{\bm{y}} +
	\underbrace{\bm{G}^T(\bm{C}+\bm{H}\bm{g})}_{\bm{C}_G} =
	\underbrace{\bm{G}^T\bm{\tau} \vphantom{()} }_{\bm{u}} 
```
which has the same algebraic form as an unconstrained system's equations of motion.

<!--
---
Kinetic energy of the system can be expressed as:
```math
	T = \frac{1}{2} \dot{\bm{y}}^T \bm{H}_G \dot{\bm{y}}
```
-->