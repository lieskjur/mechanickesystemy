# Hybrid dynamics problem of a redundantly actuated manipulator

## Least-square solution

A least-squares solution to the [hybrid dynamics problem](HybridDynamics.md)
```math
\begin{bmatrix}
	\bm{H}_{FF} & \bm{H}_{FI} \\
	\bm{H}_{IF} & \bm{H}_{II}
\end{bmatrix}
\begin{bmatrix}
    \ddot{\bm{q}}_F \\ \ddot{\bm{q}}_I
\end{bmatrix}
+
\begin{bmatrix}
    \bm{C}_F \\ \bm{C}_I
\end{bmatrix}
=
\begin{bmatrix}
    \bm{\tau}_F \\ \bm{\tau}_I
\end{bmatrix}
```
of a redundantly actuated mechanism can be found similarly to a completely actuated mechanism by calculating the pseudo inverse matrix $`\bm{H}_{FF}^{+}`$ in place of $`\bm{H}_{FF}^{-1}`$ during the *forward dynamics* step:
```math
\ddot{\bm{q}}_F = \bm{H}_{FF}^{-1} (\bm{\tau}_F - \bm{H}_{FI} \ddot{\bm{q}}_I - \bm{C}_F)
```

## Variable seperation
We will separate $`\bm{τ}_I`$ from variables which we wish to remain equal to the *least-square* solution by moving the velocity products to the right side of the EoMs and pivoting the equations around the block $`\bm{H}_{II}`$
```math
\operatorname{pinv}_{II} (\bm{H})
\begin{bmatrix}
    \ddot{\bm{q}}_F \\ \bm{τ}_I\!-\!\bm{C}_I
\end{bmatrix}
=
\begin{bmatrix}
    \bm{τ}_F\!-\!\bm{C}_F \\ \ddot{\bm{q}}_I
\end{bmatrix}
```
where $`\operatorname{pinv}_{II} (\bm{H})`$ is the  [partial pseudo inverse](../Matematika/PartialInverse.md) of $`\bm{H}`$ around the block $`\bm{H}_{II}`$
```math
\operatorname{pinv}_{II} (\bm{H})
=
\begin{bmatrix}
    \bm{H}_{FF} - \bm{H}_{FI} \bm{H}_{II}^+ \bm{H}_{IF} & \bm{H}_{FI} \bm{H}_{II}^+ \\
    - \bm{H}_{II}^+ \bm{H}_{IF}                         & \bm{H}_{II}^+
\end{bmatrix}
```
consequently we may move all variable except $`\bm{τ}_I`$ to the right side of the equations
```math
\begin{bmatrix}
    \bm{H}_{FI} \bm{H}_{II}^+ \\
    \bm{H}_{II}^+
\end{bmatrix}
\bm{τ}_I
=
\begin{bmatrix}
    \bm{τ}_F\!-\!\bm{C}_F \\ \ddot{\bm{q}}_I
\end{bmatrix}
+
\operatorname{pinv}_{II} (\bm{H})
\begin{bmatrix}
    - \ddot{\bm{q}}_F \\ \bm{C}_I
\end{bmatrix}
```

## Kernel solution
Let us define
```math
\bm{W}
=
\begin{bmatrix}
    \bm{H}_{FI} \bm{H}_{II}^+ \\
    \bm{H}_{II}^+
\end{bmatrix}
\;,\quad 
\bm{N} = \operatorname{null}(\bm{W})
```
and the kernel solution $`\bm{τ}_I^*`$
```math
\bm{τ}_I^*
=
\bm{N}\bm{u}
\;,\quad 
\bm{u} ∈ \mathbb{R}^n
\;,\quad 
n = \operatorname{nullity}(\bm{W})
```
which has no impact on the motion of the manipulator as
```math
\bm{W}\bm{N}\bm{u} = \bm{0}
```
<!-- \bm{W} ( \bm{τ}_I + \bm{τ}_I^* ) = \bm{W} \bm{τ}_I -->